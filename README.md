# DiscordChatBot

---

#### Introduction:

This ChatBot is a basic example of how a Discord Chat Bot could be implemented.
This ChatBot has no real purpose other than experimenting.

#### Know-How:

The ChatBot is made 100% in Node.js (JavaScript). For the communication with the Server the ChatBot implements the 'discord.js' API from Discord.
Every ChatBot needs to be created via https://discordbots.org/, there the Client-ID and API Token for the bot are provided.

The most difficult part is the set up and integration of the bot inside of Discord, the programming is the most easy thing to do due to the good documentation provided under https://discordjs.guide.

#### Installation & Set-Up:

As first thing the repository of this ChatBot needs to be cloned somewhere on the computer with following command:

```bat
git clone https://danilojakob@bitbucket.org/danilojakob/discordchatbot.git
```

After this is done, you need to browse into the directory where you cloned the repository earlier, this is done in the CLI with:

```bat
cd [CHATBOT_DIR]
```

When this is done the dependencies need to be installed, this can be done with the CLI by running following command:

```bat
npm install
```

If this command should not work, you need to install Node.js, this is essentially for running this project.
_After installing Node you may have to restart the computer_.

After this is done, you can start the ChatBot by typing:

```bat
node index.js
```

When you have a solid internet connection the you should be good to go with experimenting and using the ChatBot. You obvoiusly can re-use the code and make changes to your liking.

---

###### Author:

**_Danilo Jakob_**
