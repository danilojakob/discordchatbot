const MySql = require('mysql');

let connection;

/**
 * Class for the communication to the database
 */
class Database {
    
    /**
     * Method for creating the connection to the database
     * @returns Connection Object 
     */
    openConnection () {
        
        let connection = MySql.createConnection({
            host: "localhost",
            user: "root",
            password: "",
            database: "Discord"
        });

        return connection;
    };

    /**
     * Method for inserting one user into the database
     * @param {*} user 
     * @returns Nothing
     */
    registerUser (user) {
        console.log(user);
        connection = this.openConnection();
        connection.query("INSERT INTO User(ID, Username, Avatar) VALUES (?)", [user], function(err, result, fields) {
            if(err) throw err;
        });
    };
};

module.exports = Database;  