const FileSystem = require('fs');
const pathToLog = 'logs/bot.log';
/**
 * Class for logging
 */
class Logger {
    /**
     * Method for writing log
     * @param {string} text 
     */
    writeLine(text) {
        var today = new Date();
        text = String(today + ": " + text)
        FileSystem.appendFile(pathToLog, text, function(err) {
            if (err) throw err;
        });
    }
}

module.exports = Logger;