//Discord
const Discord = require("discord.js");
const client = new Discord.Client();
// Database
const Database = require("./modules/Database");
const database = new Database();
//Logger
const Logger = require('./modules/Logger');
const logger = new Logger();

const welcomeMessages = [
  "Welcome to the server, ",
  "Say hello to the new cunt, ",
  "Can you go, ",
  "Random dude appeared, "
]

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

// Get the Ehrenkfaktor
client.on("message", msg => {
  if (msg.content === "!ehre") {
    logger.writeLine(`User: ${msg.author.username} wants Ehrenfaktor\n`);
    const faktor = generateEhrenfaktor();
    msg.reply(msg.author.username + " hat einen Ehrenfaktor von " + faktor);
  }
});

/**
 * Method for help about commands
 */
client.on("message", msg => {
  if (msg.content === "!info") {
    logger.writeLine(`User: ${msg.author.username} wants Info about commands\n`);
    const channel = msg.channel;
    const embedInfo = new Discord.RichEmbed()
      .setColor('#0099ff')
      .setTitle('Info')
      .setAuthor('DiscordChatBot', 'https://cdn.discordapp.com/avatars/381876134742327313/93d924ecad20a755b16012fa8a03e0f9.png?size=2048')
      .setDescription('Info about available commands')
      .addBlankField()
      .addField('Ehrenfaktor', '!ehre', true)
      .addField('Help', '!info', true)
      .addField('Kick User', '!kick @[USER]', true)
      .addField('Info about me', '!me', true);
    channel.send(embedInfo);
  }
});

/**
 * Method to get User information
 */
client.on("message", msg => {
  const roles = msg.member.roles.map(m => m.name);
  if (msg.content === "!me") {
    
    logger.writeLine(`User: ${msg.author.username} wants Info about himself\n`);
    const embedMe = new Discord.RichEmbed() 
    .setColor('#0099ff')
    .setTitle('Me')
    .setAuthor('DiscordChatBot', 'https://cdn.discordapp.com/avatars/381876134742327313/93d924ecad20a755b16012fa8a03e0f9.png?size=2048')
    .setDescription('Info about me')
    .addBlankField()
    .addField('Username', msg.author.username)
    .addField('User ID', msg.member.user.id)
    .addField('Roles', roles)

    msg.reply(embedMe);
  }
});

/**
 * Function to kick players
 */
client.on("message", msg => {
  if (msg.content.startsWith("!kick")) {
    const user = msg.mentions.users.first();
    logger.writeLine(`User: ${msg.author.username} wants to kick ${msg.guild.member(user)}\n`);
    if (user) {
      //Get the user Id from who is writing
      const writerId = msg.author.id;
      //Get the member
      const member = msg.guild.member(user);
      //Check if the user who is writing has admin role
      const isAdmin = msg.guild.members
        .get(writerId)
        .roles.exists("name", "@admins");
      //Check if member does exist and that the user who is writing is admin
      if (member && isAdmin) {
        member
          .kick("You have been kicked by the admin for a violation!")
          .then(() => {
            msg.reply("User " + member + " has been kicked");
          })
          .catch(err => {
            msg.reply("Cannot kick User");
          });
      } else {
        //If the member does not exist, print out right error message and if the user has not the right role
        msg.reply(
          !member
            ? "Member does not exist!"
            : "You don't have the permission to execute this command!"
        );
      }
    }
  }
});

/*
 *   Welcome Message for new members
 */
client.on("guildMemberAdd", member => {
  // Search for the welcome Channel
  const channel = member.guild.channels.find(ch => ch.name === "welcome");
  // Do nothing if the channel wasn't found on this server
  if (!channel) return;
  // Send the message, mentioning the member
  const user = [member.user.id, member.user.username, member.user.avatarURL];
  //Insert user into the database
  database.registerUser(user);
  channel.send(`${welcomeMessages[getRandomIndexFromArray(welcomeMessages.length)]}, ${member}`);
});

/**
 * Funtion to generate a random Ehrenkfaktor
 * @returns Ehrenfaktor
 */
function generateEhrenfaktor() {
  return Math.round(Math.random() * 10) + 1;
}

/**
 * Function to generate a random index for an array
 */
function getRandomIndexFromArray(arrayLength) {
  return Math.round(Math.random() * arrayLength);
}

//API Key for login
client.login("NTkwNTk4NzY0OTM1MjQ5OTUx.XQkktg.IfbkME5XKFxtmKkn6NHIdALxcRs");
